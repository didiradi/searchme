// Generated code from Butter Knife. Do not modify!
package net.marusoft.common;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class Common_List_Adapter$ViewHolder$$ViewBinder<T extends net.marusoft.common.Common_List_Adapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492991, "field 'cl_iv'");
    target.cl_iv = finder.castView(view, 2131492991, "field 'cl_iv'");
    view = finder.findRequiredView(source, 2131492992, "field 'cl_title_tv'");
    target.cl_title_tv = finder.castView(view, 2131492992, "field 'cl_title_tv'");
    view = finder.findRequiredView(source, 2131492993, "field 'cl_content_tv'");
    target.cl_content_tv = finder.castView(view, 2131492993, "field 'cl_content_tv'");
    view = finder.findRequiredView(source, 2131492994, "field 'cl_money_tv'");
    target.cl_money_tv = finder.castView(view, 2131492994, "field 'cl_money_tv'");
  }

  @Override public void unbind(T target) {
    target.cl_iv = null;
    target.cl_title_tv = null;
    target.cl_content_tv = null;
    target.cl_money_tv = null;
  }
}
