// Generated code from Butter Knife. Do not modify!
package net.marusoft.lost_and_found;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LostAndFound_Fragment$$ViewBinder<T extends net.marusoft.lost_and_found.LostAndFound_Fragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492990, "field 'cl_recycleView'");
    target.cl_recycleView = finder.castView(view, 2131492990, "field 'cl_recycleView'");
  }

  @Override public void unbind(T target) {
    target.cl_recycleView = null;
  }
}
