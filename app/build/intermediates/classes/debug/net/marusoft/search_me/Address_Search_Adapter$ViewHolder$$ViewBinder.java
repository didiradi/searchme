// Generated code from Butter Knife. Do not modify!
package net.marusoft.search_me;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class Address_Search_Adapter$ViewHolder$$ViewBinder<T extends net.marusoft.search_me.Address_Search_Adapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492984, "field 'search_adapter_tv'");
    target.search_adapter_tv = finder.castView(view, 2131492984, "field 'search_adapter_tv'");
  }

  @Override public void unbind(T target) {
    target.search_adapter_tv = null;
  }
}
