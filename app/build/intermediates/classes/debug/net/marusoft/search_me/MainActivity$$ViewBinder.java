// Generated code from Butter Knife. Do not modify!
package net.marusoft.search_me;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MainActivity$$ViewBinder<T extends net.marusoft.search_me.MainActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492973, "field 'main_address_tv'");
    target.main_address_tv = finder.castView(view, 2131492973, "field 'main_address_tv'");
    view = finder.findRequiredView(source, 2131492975, "method 'ibtn1'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.ibtn1();
        }
      });
    view = finder.findRequiredView(source, 2131492976, "method 'ibtn2'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.ibtn2();
        }
      });
    view = finder.findRequiredView(source, 2131492977, "method 'ibtn3'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.ibtn3();
        }
      });
    view = finder.findRequiredView(source, 2131492978, "method 'ibtn4'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.ibtn4();
        }
      });
    view = finder.findRequiredView(source, 2131492979, "method 'ibtn5'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.ibtn5();
        }
      });
    view = finder.findRequiredView(source, 2131492980, "method 'ibtn6'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.ibtn6();
        }
      });
    view = finder.findRequiredView(source, 2131492974, "method 'search_ibtn'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.search_ibtn();
        }
      });
  }

  @Override public void unbind(T target) {
    target.main_address_tv = null;
  }
}
