// Generated code from Butter Knife. Do not modify!
package net.marusoft.search_me;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class Address_Search_Activity$$ViewBinder<T extends net.marusoft.search_me.Address_Search_Activity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492983, "field 'search_recycleView'");
    target.search_recycleView = finder.castView(view, 2131492983, "field 'search_recycleView'");
    view = finder.findRequiredView(source, 2131492981, "field 'search_address_et'");
    target.search_address_et = finder.castView(view, 2131492981, "field 'search_address_et'");
    view = finder.findRequiredView(source, 2131492982, "method 'sa_btn'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.sa_btn();
        }
      });
  }

  @Override public void unbind(T target) {
    target.search_recycleView = null;
    target.search_address_et = null;
  }
}
