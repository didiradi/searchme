package net.marusoft.wanted;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import net.marusoft.search_me.BaseActivity;
import net.marusoft.search_me.R;

/**
 * Created by MARUSOFT-CHOI on 2016-01-28.
 */
public class Wanted_MainActivity extends BaseActivity {
    public ViewPager viewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_viewpager);

        viewPager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

    }


    public class PagerAdapter extends FragmentStatePagerAdapter {
        final int PAGE_COUNT = 6;

        private final String[] TITLES = {"살인", "강도", "강간", "절도", "폭력", "기타"};


        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(View container, int position, Object object) {

            // TODO Auto-generated method stub
            super.destroyItem(container, position, object);

        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                Fragment fragment = new Wanted_Murder_Fragment();

                return fragment;
            } else if (position == 1) {
                Fragment fragment = new Wanted_Strength_Fragment();

                return fragment;
            } else if (position == 2) {
                Fragment fragment = new Wanted_Rape_Fragment();

                return fragment;
            } else if (position == 3) {
                Fragment fragment = new Wanted_Theft_Fragment();

                return fragment;
            }  else if (position == 4) {
                Fragment fragment = new Wanted_Violence_Fragment();

                return fragment;
            } else {
                Fragment fragment = new Wanted_etc_Fragment();

                return fragment;
            }
        }
    }
}
