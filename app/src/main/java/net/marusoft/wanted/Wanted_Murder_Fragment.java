package net.marusoft.wanted;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.marusoft.common.Common_List_Adapter;
import net.marusoft.common.Common_List_Data;
import net.marusoft.search_me.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by MARUSOFT-CHOI on 2016-01-28. 살인
 */
public class Wanted_Murder_Fragment extends Fragment {
    @Bind(R.id.cl_recycleView)
    RecyclerView cl_recycleView;
    Common_List_Adapter adapter;
    ArrayList<Common_List_Data> data_list = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.common_list_fragment, container, false);
        ButterKnife.bind(this, v);
        data_list.add(new Common_List_Data("111", "111", "111", 1));
        data_list.add(new Common_List_Data("222", "222", "222", 2));
        data_list.add(new Common_List_Data("333", "333", "333", 3));
        data_list.add(new Common_List_Data("444", "444", "444", 4));
        data_list.add(new Common_List_Data("555", "555", "555", 5));
        data_list.add(new Common_List_Data("111", "111", "111", 1));
        data_list.add(new Common_List_Data("222", "222", "222", 2));
        data_list.add(new Common_List_Data("333", "333", "333", 3));
        data_list.add(new Common_List_Data("444", "444", "444", 4));
        data_list.add(new Common_List_Data("555", "555", "555", 5));
        data_list.add(new Common_List_Data("111", "111", "111", 1));
        data_list.add(new Common_List_Data("222", "222", "222", 2));
        data_list.add(new Common_List_Data("333", "333", "333", 3));
        data_list.add(new Common_List_Data("444", "444", "444", 4));
        data_list.add(new Common_List_Data("555", "555", "555", 5));
        data_list.add(new Common_List_Data("111", "111", "111", 1));

        adapter = new Common_List_Adapter(getActivity(), data_list, R.layout.common_list_fragment);
        cl_recycleView.setHasFixedSize(true);
        cl_recycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        cl_recycleView.setAdapter(adapter);

        return v;
    }
}
