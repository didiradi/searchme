package net.marusoft.witness;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import net.marusoft.search_me.BaseActivity;
import net.marusoft.search_me.R;

/**
 * Created by MARUSOFT-CHOI on 2016-01-28.
 */
public class Witness_MainActivity extends BaseActivity {
    public ViewPager viewPager;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_viewpager);
        viewPager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
    }


    public class PagerAdapter extends FragmentStatePagerAdapter {
        final int PAGE_COUNT = 3;

        private final String[] TITLES = {"교통사고", "폭행", "기타"};

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(View container, int position, Object object) {

            // TODO Auto-generated method stub
            super.destroyItem(container, position, object);

        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                Fragment fragment = new Witness_Accident_Fragment();

                return fragment;
            } else if (position == 1) {
                Fragment fragment = new Witness_Violence_Fragment();

                return fragment;
            } else {
                Fragment fragment = new Witness_etc_Fragment();

                return fragment;
            }
        }
    }
}
