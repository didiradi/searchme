package net.marusoft.search_me;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import net.marusoft.lost_and_found.LostAndFound_Fragment;
import net.marusoft.lost_and_found.LostAndFound_MainActivity;
import net.marusoft.wanted.Wanted_MainActivity;
import net.marusoft.witness.Witness_MainActivity;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import util.GpsInfo;
import util.MyLogs;
import util.MyProgressDialog;

public class MainActivity extends AppCompatActivity {
    /*@Bind(R.id.main_ibtn1) ImageButton main_ibtn1; // 현상범
    @Bind(R.id.main_ibtn2) ImageButton main_ibtn2; // 목격자를 찾습니다.
    @Bind(R.id.main_ibtn3) ImageButton main_ibtn3; // 사람을 찾습니다.
    @Bind(R.id.main_ibtn4) ImageButton main_ibtn4; // 제보를 받습니다.
    @Bind(R.id.main_ibtn5) ImageButton main_ibtn5; // 분실물
    @Bind(R.id.main_ibtn6) ImageButton main_ibtn6; // 실종*/
    @Bind(R.id.main_address_tv) TextView main_address_tv;
    public static final String API_URL = "https://apis.daum.net";
    public static final int SEARCH_TIME = 10;
    public GpsInfo gps;
    private TimerTask mTask;
    private Timer mTimer;
    private int timer_value = 0;
    public double latitude = 0.0;
    public double longitude = 0.0;
    public Context context;
    private Address_Data address_data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        ButterKnife.bind(this);
        chkGpsService();
    }

    @OnClick(R.id.main_ibtn1)
    void ibtn1() {
        Toast.makeText(this, "111", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, Wanted_MainActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.main_ibtn2)
    void ibtn2() {
        Toast.makeText(this, "222", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, Witness_MainActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.main_ibtn3)
    void ibtn3() {
        //분실물
        Toast.makeText(this, "333", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, LostAndFound_MainActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.main_ibtn4)
    void ibtn4() {
        Toast.makeText(this, "444", Toast.LENGTH_SHORT).show();
    }
    @OnClick(R.id.main_ibtn5)
    void ibtn5() {
        Toast.makeText(this, "555", Toast.LENGTH_SHORT).show();
    }
    @OnClick(R.id.main_ibtn6)
    void ibtn6() {
        Toast.makeText(this, "666", Toast.LENGTH_SHORT).show();
    }
    @OnClick(R.id.address_search_ibtn)
    void search_ibtn(){
        Intent intent = new Intent(context, Address_Search_Activity.class);
        startActivityForResult(intent, 1);
    }

    //GPS 설정 체크
    private boolean chkGpsService() {
        gps = new GpsInfo(context);
        String c_gps = android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (!(c_gps.matches(".*gps.*") && c_gps.matches(".*network.*"))) {

            // GPS OFF 일때 Dialog 표시
            AlertDialog.Builder gsDialog = new AlertDialog.Builder(this);
            gsDialog.setTitle("위치 서비스 설정");
            gsDialog.setCancelable(false);
            gsDialog.setMessage("무선 네트워크 사용, GPS 위성 사용을 모두 체크하셔야 정확한 위치 서비스가 가능합니다.\n위치 서비스 기능을 설정하시겠습니까?");
            gsDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // GPS설정 화면으로 이동
                    Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    startActivityForResult(intent, 0);
                }
            })
                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    }).create().show();
            return false;

        } else {
            gps = new GpsInfo(context);
            get_gpsjson();
            Toast.makeText(context, ""+gps.getLatitude() + gps.getLongitude(), Toast.LENGTH_SHORT).show();
            return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if(resultCode == 0 || resultCode == -1) { //위치정보창에서 받는 리턴값
                    MyLogs.v("들어옴");
                    // GPS 사용유무 가져오기
                    MyProgressDialog.showDialog(context, "현재 위치를 찾는중입니다.");
                    mTask = new TimerTask() {
                        @Override
                        public void run() {
                            if(timer_value < SEARCH_TIME) { //10초동안 탐색
                                if(latitude == 0.0) { //gps안잡힐때
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            runOnUiThread(new Runnable(){
                                                @Override
                                                public void run() {
                                                    gps = new GpsInfo(context);
                                                    latitude = gps.getLatitude();
                                                    longitude = gps.getLongitude();
                                                    timer_value++;
                                                }
                                            });
                                        }
                                    }).start();
                                } else {
                                    mTimer.cancel();
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            runOnUiThread(new Runnable(){
                                                @Override
                                                public void run() {
                                                    MyProgressDialog.disMissDialog();
                                                    mTimer.cancel();
                                                    Toast.makeText(context, "위치가 잡혔습니다.!" + gps.getLatitude() + " : " + gps.getLongitude(), Toast.LENGTH_SHORT).show();
                                                    get_gpsjson();
                                                }
                                            });
                                        }
                                    }).start();
                                }
                            } else {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        runOnUiThread(new Runnable(){
                                            @Override
                                            public void run() {
                                                MyProgressDialog.disMissDialog();
                                                mTimer.cancel();
                                                Toast.makeText(context, "시간이 초과되어 앱을 다시 실행 하여주세요!", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                }).start();

                                //finish();
                            }

                        }
                    };

                    mTimer = new Timer();

                    mTimer.schedule(mTask, 1000, 1000);
                }

                break;
            case 1:
                if(resultCode == RESULT_OK) {
                    String address_title = data.getStringExtra("address_title");
                    MyLogs.v("address_title : " + address_title);
                    main_address_tv.setText(address_title);
                }
                break;
            default:


                break;
        }
    }

    Address_Data get_gpsjson() {
        address_data = new Address_Data();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<Address_Data> call = service.addressdata("bed981887c7a5510722065105fe72fd7", gps.getLongitude(), gps.getLatitude(), "WGS84", "json");
        call.enqueue(new Callback<Address_Data>() {
            @Override
            public void onResponse(Response<Address_Data> response, Retrofit retrofit) {
                if(response.isSuccess()) {
                    address_data = response.body();
                    MyLogs.v("retrofit data : " + address_data.toString());

                    main_address_tv.setText(address_data.getFullName());
                } else {

                }
            }

            @Override
            public void onFailure(Throwable t) {
                MyLogs.v("Fail");
            }
        });
        return address_data;
    }

    @Override
    protected void onDestroy() {
        MyLogs.v("timer destroy");
        if(mTimer!=null) {
            mTimer.cancel();
        }
        super.onDestroy();
    }


}
