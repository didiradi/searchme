package net.marusoft.search_me;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import util.MyLogs;

/**
 * Created by MARUSOFT-CHOI on 2016-01-29.
 */
public class Address_Search_Adapter extends RecyclerView.Adapter<Address_Search_Adapter.ViewHolder> {
    private Address_Point_Data items;
    Context context;
    int item_layout;

    public Address_Search_Adapter(Context context, Address_Point_Data items, int item_layout) {
        this.context=context;
        this.items=items;
        this.item_layout=item_layout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.address_search_item, null);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Address_Point_Data.Channel.Item item = items.getChannel().getItem().get(position);
        MyLogs.v("test" + item.toString());
        holder.search_adapter_tv.setText(item.getTitle());


    }


    @Override
    public int getItemCount() {
        return this.items.getChannel().getItem().size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.search_adapter_tv) TextView search_adapter_tv;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.putExtra("address_title", items.getChannel().getItem().get(getPosition()).getTitle());
                    ((Address_Search_Activity)context).setResult(((Address_Search_Activity) context).RESULT_OK, intent);
                    ((Address_Search_Activity) context).finish();
                }
            });
        }
    }
}
