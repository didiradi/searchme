package net.marusoft.search_me;


import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

/**
 * Created by MARUSOFT-CHOI on 2016-01-28.
 */
public class Main_ViewPager extends FragmentActivity implements ViewPager.OnPageChangeListener {
    public ViewPager viewPager;

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
