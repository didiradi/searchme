package net.marusoft.search_me;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import util.MyLogs;

/**
 * Created by MARUSOFT-CHOI on 2016-01-29.
 */
public class Address_Search_Activity extends Activity {
    @Bind(R.id.search_recycleView)
    RecyclerView search_recycleView;
    @Bind(R.id.search_address_et)
    EditText search_address_et;
    private Context context;
    private Address_Search_Adapter adapter;
    private Address_Point_Data data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address_search_activity);
        ButterKnife.bind(this);
        context = this;
    }

    @OnClick(R.id.search_address_btn)
    void sa_btn() {
        get_addressjson(search_address_et.getText().toString());
    }

    Address_Point_Data get_addressjson(String address_value) {
        data = new Address_Point_Data();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<Address_Point_Data> call = service.address_point_data("bed981887c7a5510722065105fe72fd7", address_value, 30, "json");
        call.enqueue(new Callback<Address_Point_Data>() {
            @Override
            public void onResponse(Response<Address_Point_Data> response, Retrofit retrofit) {
                if(response.isSuccess()) {
                    data = response.body();
                    adapter = new Address_Search_Adapter(Address_Search_Activity.this, data, R.layout.activity_main);
                    search_recycleView.setHasFixedSize(true);
                    search_recycleView.setLayoutManager(new LinearLayoutManager(context));
                    search_recycleView.setAdapter(adapter);
                    MyLogs.v("retrofit point data : " + data.getChannel().getItem().toString());
                } else {

                }
            }

            @Override
            public void onFailure(Throwable t) {
                MyLogs.v("Fail");
            }
        });
        return data;
    }
}
