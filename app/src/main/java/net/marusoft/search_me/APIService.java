package net.marusoft.search_me;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by CHOI on 2016-01-29.
 */
public interface APIService {
    @GET("/local/geo/coord2addr")
    Call<Address_Data> addressdata(
            @Query("apikey") String apikey,
            @Query("longitude") Double longitude,
            @Query("latitude") Double latitude,
            @Query("inputCoordSystem") String inputCoordSystem,
            @Query("output") String output);

    @GET("/local/geo/addr2coord")
    Call<Address_Point_Data> address_point_data(
            @Query("apikey") String apikey,
            @Query("q") String q,
            @Query("page_size") int page_size,
            @Query("output") String output);
}
