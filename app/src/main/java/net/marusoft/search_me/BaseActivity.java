package net.marusoft.search_me;


import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by MARUSOFT-CHOI And Henen on 2016-01-28.
 */
public class BaseActivity extends AppCompatActivity {
    public Retrofit retrofit;
    public APIService service;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(APIService.class);
    }


}
