package net.marusoft.common;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import net.marusoft.search_me.BaseActivity;
import net.marusoft.search_me.R;
import net.marusoft.wanted.Wanted_Murder_Fragment;
import net.marusoft.wanted.Wanted_Rape_Fragment;
import net.marusoft.wanted.Wanted_Strength_Fragment;
import net.marusoft.wanted.Wanted_Theft_Fragment;
import net.marusoft.wanted.Wanted_Violence_Fragment;
import net.marusoft.wanted.Wanted_etc_Fragment;

import butterknife.ButterKnife;

/**
 * Created by MARUSOFT-CHOI on 2016-01-29.
 */
public class Common_List_Detail_Activity extends BaseActivity {
    Context context;
    public ViewPager viewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_list_detail_activity);
        ButterKnife.bind(this);
        context = this;

        viewPager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
    }


    public class PagerAdapter extends FragmentStatePagerAdapter {
        final int PAGE_COUNT = 6;

        private final String[] TITLES = {"살인", "강도", "강간", "절도", "폭력", "기타"};


        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(View container, int position, Object object) {

            // TODO Auto-generated method stub
            super.destroyItem(container, position, object);

        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                Fragment fragment = new Wanted_Murder_Fragment();

                return fragment;
            } else if (position == 1) {
                Fragment fragment = new Wanted_Strength_Fragment();

                return fragment;
            } else if (position == 2) {
                Fragment fragment = new Wanted_Rape_Fragment();

                return fragment;
            } else if (position == 3) {
                Fragment fragment = new Wanted_Theft_Fragment();

                return fragment;
            }  else if (position == 4) {
                Fragment fragment = new Wanted_Violence_Fragment();

                return fragment;
            } else {
                Fragment fragment = new Wanted_etc_Fragment();

                return fragment;
            }
        }
    }
}
