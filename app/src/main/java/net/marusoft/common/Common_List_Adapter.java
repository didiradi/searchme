package net.marusoft.common;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.marusoft.search_me.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import util.MyLogs;

/**
 * Created by MARUSOFT-CHOI on 2016-01-29.
 */
public class Common_List_Adapter extends RecyclerView.Adapter<Common_List_Adapter.ViewHolder>{
    private ArrayList<Common_List_Data> items;
    Context context;
    int item_layout;

    public Common_List_Adapter(Context context, ArrayList<Common_List_Data> items, int item_layout) {
        this.context=context;
        this.items=items;
        this.item_layout=item_layout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.common_list_item, null);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Common_List_Data item = items.get(position);
        MyLogs.v("c_data" + item.toString());
        Glide.with(context).load(R.drawable.menu1_off).into(holder.cl_iv);
        holder.cl_title_tv.setText(item.getC_title());
        holder.cl_content_tv.setText(item.getC_content());
        holder.cl_money_tv.setText(""+item.getC_money());
    }


    @Override
    public int getItemCount() {
        return this.items.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.cl_iv)
        ImageView cl_iv;
        @Bind(R.id.cl_title_tv)
        TextView cl_title_tv;
        @Bind(R.id.cl_content_tv)
        TextView cl_content_tv;
        @Bind(R.id.cl_money_tv)
        TextView cl_money_tv;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, Common_List_Detail_Activity.class);
                    context.startActivity(intent);
                }
            });
        }
    }
}
