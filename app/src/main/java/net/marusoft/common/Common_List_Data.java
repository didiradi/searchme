package net.marusoft.common;

/**
 * Created by MARUSOFT-CHOI on 2016-01-29.
 */
public class Common_List_Data {
    String c_img;
    String c_title;
    String c_content;
    int c_money;

    public Common_List_Data(String c_img, String c_title, String c_content, int c_money) {
        this.c_img = c_img;
        this.c_title = c_title;
        this.c_content = c_content;
        this.c_money = c_money;
    }

    public String getC_img() {
        return c_img;
    }

    public void setC_img(String c_img) {
        this.c_img = c_img;
    }

    public String getC_title() {
        return c_title;
    }

    public void setC_title(String c_title) {
        this.c_title = c_title;
    }

    public String getC_content() {
        return c_content;
    }

    public void setC_content(String c_content) {
        this.c_content = c_content;
    }

    public int getC_money() {
        return c_money;
    }

    public void setC_money(int c_money) {
        this.c_money = c_money;
    }

    @Override
    public String toString() {
        return "Common_List_Data{" +
                "c_img='" + c_img + '\'' +
                ", c_title='" + c_title + '\'' +
                ", c_content='" + c_content + '\'' +
                ", c_money=" + c_money +
                '}';
    }
}
