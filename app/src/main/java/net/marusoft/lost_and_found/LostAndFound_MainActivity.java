package net.marusoft.lost_and_found;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import net.marusoft.search_me.BaseActivity;
import net.marusoft.search_me.R;

import java.util.ArrayList;

/**
 * Created by MARUSOFT-Henen on 2016-01-28.
 */
public class LostAndFound_MainActivity extends BaseActivity {
    public ViewPager viewPager;
    public TabLayout tabLayout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_viewpager);
        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout)findViewById(R.id.tabs);
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(adapter.getCount()); // page 확정
        tabLayout.setupWithViewPager(viewPager);
    }


    public class PagerAdapter extends FragmentStatePagerAdapter {
        ArrayList<LostAndFound_Fragment> lostAndFound_fragmentArrayList;

        final int PAGE_COUNT = LostAndFoundCategory.count();

        private final String[] TITLES = LostAndFoundCategory.get_title_array();

        public PagerAdapter(FragmentManager fm) {
            super(fm);
            lostAndFound_fragmentArrayList = new ArrayList<>();
            for(int i = 0  ; i<LostAndFoundCategory.count();i++ ){
                lostAndFound_fragmentArrayList.add(new LostAndFound_Fragment().newInstance(LostAndFoundCategory.getCategoryItem(i)));
            }
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(View container, int position, Object object) {

            // TODO Auto-generated method stub
            super.destroyItem(container, position, object);

        }

        @Override
        public Fragment getItem(int position) {
            return lostAndFound_fragmentArrayList.get(position);
                    /*
            if (position == 0) {


               ;
            } else if (position == 1) {


                return fragment;
            } else {


                return fragment;
            }
            */
        }
    }
}
