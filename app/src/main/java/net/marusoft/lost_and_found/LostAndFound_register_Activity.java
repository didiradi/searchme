package net.marusoft.lost_and_found;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.soundcloud.android.crop.Crop;
import com.tispoon.henen.HenenBoard.HorizontalImageLayoutAdapter;
import net.marusoft.search_me.BaseActivity;
import net.marusoft.search_me.R;
import java.io.File;
import util.MyLogs;

/**
 * Created by Henen on 2016-02-01.
 */
public class LostAndFound_register_Activity extends BaseActivity {
    private int imageCount = 0;
    private RecyclerView mRecyclerView;
    HorizontalImageLayoutAdapter horizontalImageLayoutAdapter;
    Button addImageBtn;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lostandfound_register_activity);
        addImageBtn = (Button)findViewById(R.id.image_add_button);
        mRecyclerView = (RecyclerView)findViewById(R.id.list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(LostAndFound_register_Activity.this,LinearLayoutManager.HORIZONTAL,false);
        horizontalImageLayoutAdapter = new HorizontalImageLayoutAdapter(LostAndFound_register_Activity.this,mRecyclerView,R.layout.horizontalimageitem);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(horizontalImageLayoutAdapter);
        addImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Crop.pickImage(LostAndFound_register_Activity.this);
            }
        });
    }





    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
            beginCrop(result.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, result);
        }
        super.onActivityResult(requestCode, resultCode, result);
    }

    private void beginCrop(Uri source) {
        imageCount++;
        Uri destination = Uri.fromFile(new File(getCacheDir(), imageCount+"img.png"));
        Crop.of(source, destination).asSquare().withMaxSize(120,120).start(LostAndFound_register_Activity.this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            MyLogs.v("OK");
            Toast.makeText(LostAndFound_register_Activity.this, "OK", Toast.LENGTH_SHORT).show();

            File file = new File(Crop.getOutput(result).getPath());
            MyLogs.v(file.getAbsolutePath());
            MyLogs.v(file.getName());
            horizontalImageLayoutAdapter.addItem(Crop.getOutput(result));
            //TypedFile typedFile = new TypedFile("multipart/form-data",file);
            //TypedFile typedFile = new TypedFile("multipart/form-data",file);


        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(LostAndFound_register_Activity.this,"Error",Toast.LENGTH_SHORT).show();
            MyLogs.v("OK");
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.board_finish_fragment_menu,menu);
       return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.finish_btn) {
            //올리면됨
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
