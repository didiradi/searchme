package net.marusoft.lost_and_found;

import java.io.Serializable;

/**
 * Created by Henen on 2016-02-01.
 */
public class LostAndFoundCategoryItem implements Serializable{

    private int id;
    private String title;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public LostAndFoundCategoryItem(int id, String title) {
        this.id = id;
        this.title = title;
    }
}
