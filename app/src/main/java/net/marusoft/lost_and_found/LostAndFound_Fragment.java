package net.marusoft.lost_and_found;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import net.marusoft.common.Common_List_Adapter;
import net.marusoft.common.Common_List_Data;
import net.marusoft.search_me.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by MARUSOFT-CHOI on 2016-01-28. 분실물 프래그먼트
 */
public class LostAndFound_Fragment extends Fragment {


    private static final String ARG_CATEGORY = "CATEGORY";
    private LostAndFoundCategoryItem PARAM_CATEGORY;

    public LostAndFound_Fragment newInstance(LostAndFoundCategoryItem categoryItem) {
        LostAndFound_Fragment lostAndFound_Fragment = new LostAndFound_Fragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_CATEGORY,categoryItem);
        lostAndFound_Fragment.setArguments(args);
        return lostAndFound_Fragment;
    }

    @Bind(R.id.cl_recycleView)
    RecyclerView cl_recycleView;
    Common_List_Adapter adapter;
    ArrayList<Common_List_Data> data_list = new ArrayList<>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            PARAM_CATEGORY  = (LostAndFoundCategoryItem)getArguments().getSerializable(ARG_CATEGORY);
            setHasOptionsMenu(true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.common_list_fragment, container, false);
        ButterKnife.bind(this, v);
        data_list.add(new Common_List_Data("111", "111", "111", 1));
        data_list.add(new Common_List_Data("222", "222", "222", 2));
        data_list.add(new Common_List_Data("333", "333", "333", 3));
        data_list.add(new Common_List_Data("444", "444", "444", 4));
        data_list.add(new Common_List_Data("555", "555", "555", 5));
        data_list.add(new Common_List_Data("111", "111", "111", 1));
        data_list.add(new Common_List_Data("222", "222", "222", 2));
        data_list.add(new Common_List_Data("333", "333", "333", 3));
        data_list.add(new Common_List_Data("444", "444", "444", 4));
        data_list.add(new Common_List_Data("555", "555", "555", 5));
        data_list.add(new Common_List_Data("111", "111", "111", 1));
        data_list.add(new Common_List_Data("222", "222", "222", 2));
        data_list.add(new Common_List_Data("333", "333", "333", 3));
        data_list.add(new Common_List_Data("444", "444", "444", 4));
        data_list.add(new Common_List_Data("555", "555", "555", 5));
        data_list.add(new Common_List_Data("111", "111", "111", 1));

        adapter = new Common_List_Adapter(getActivity(), data_list, R.layout.common_list_fragment);
        cl_recycleView.setHasFixedSize(true);
        cl_recycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        cl_recycleView.setAdapter(adapter);

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.board_register_fragment_menu, menu);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.register_btn) {
            Intent intent = new Intent(getActivity(),LostAndFound_register_Activity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



}
