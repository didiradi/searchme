package net.marusoft.lost_and_found;

import java.util.ArrayList;

/**
 * Created by SEO on 2016-02-01.
 */
public class LostAndFoundCategory {
    private static ArrayList<LostAndFoundCategoryItem> lostAndFoundCategoryItemArrayList = null;
    private  static void init(){
        if(lostAndFoundCategoryItemArrayList==null){
            lostAndFoundCategoryItemArrayList = new ArrayList<>();
            lostAndFoundCategoryItemArrayList.add(new LostAndFoundCategoryItem(1,"휴대폰"));
            lostAndFoundCategoryItemArrayList.add(new LostAndFoundCategoryItem(2,"가방"));
            lostAndFoundCategoryItemArrayList.add(new LostAndFoundCategoryItem(3,"지갑"));
            lostAndFoundCategoryItemArrayList.add(new LostAndFoundCategoryItem(4,"직접등록"));
        }
    }

    public static LostAndFoundCategoryItem getCategoryItem(int position){
        LostAndFoundCategoryItem result_item = null;
        LostAndFoundCategory.init();
        result_item =  lostAndFoundCategoryItemArrayList.get(position);
        return result_item;
    }


    public static int getCategoryId(String title){
        int result_id = -1;
        LostAndFoundCategory.init();
        for(int i  = 0 ;  i<lostAndFoundCategoryItemArrayList.size();i++){
            if(lostAndFoundCategoryItemArrayList.get(i).getTitle().equals(title)){
                result_id = lostAndFoundCategoryItemArrayList.get(i).getId();
                break;
            }
        }

        return result_id;
    }

    public static String getCategoryTitle(int id){
        String result_title = "";
        LostAndFoundCategory.init();
        for(int i  = 0 ;  i<lostAndFoundCategoryItemArrayList.size();i++){
            if(lostAndFoundCategoryItemArrayList.get(i).getId() == id){
                result_title = lostAndFoundCategoryItemArrayList.get(i).getTitle();
                break;
            }
        }
        return result_title;
    }

    public static String [] get_title_array(){
        String [] title_array = new String [lostAndFoundCategoryItemArrayList.size()];
        LostAndFoundCategory.init();
        for(int i  = 0 ;  i<lostAndFoundCategoryItemArrayList.size();i++){
            title_array[i] = lostAndFoundCategoryItemArrayList.get(i).getTitle();

        }
        return title_array;
    }


    public static int count(){
        init();
        return lostAndFoundCategoryItemArrayList.size();
    }

}
