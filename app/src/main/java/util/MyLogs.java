package util;

import android.util.Log;

import java.util.Locale;

/**
 * Created by CHOI on 2016-01-28.
 */
public class MyLogs {
    public final static String LOGTAG = "MyLog";
    static final boolean LOGV = true;

    public static void v(String logMe) {
        if (LOGV) {
            try {
                throw new Exception("go go logger");
            } catch (Exception e) {
                String className = e.getStackTrace()[1].getClassName();
                className = className.substring(className.lastIndexOf(".") + 1, className.length());
                android.util.Log.v(LOGTAG, className + "." + e.getStackTrace()[1].getMethodName() + "() --- " + logMe);
            }
        }
    }
}
