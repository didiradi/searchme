package com.tispoon.henen.HenenBoard;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;


import net.marusoft.search_me.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yoon on 2015-12-10.
 */
public class HorizontalImageLayoutAdapter extends RecyclerView.Adapter<HorizontalImageLayoutAdapter.CustomViewHolder>{



    private final Context mContext;
    private final RecyclerView mRecyclerView;
    private final List<Uri> mItems;
    private final int mLayoutId;
    private int mCurrentItemId = 0;


    public HorizontalImageLayoutAdapter(Context context, RecyclerView recyclerView, int layoutId) {
        mContext = context;
        mItems = new ArrayList<>();
        mRecyclerView = recyclerView;
        mLayoutId = layoutId;
    }



    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.horizontalimageitem, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position) {
        holder.previmageView.setImageURI(mItems.get(position));
        holder.deleteImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeItem(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void addItem(Uri uri) {
        mItems.add(uri);
        notifyItemInserted(mItems.size()-1);
    }

    public void removeItem(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }


    public List<Uri> getmItems() {
        return mItems;
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        public final ImageView previmageView;
        public final ImageButton deleteImgBtn;
        public CustomViewHolder(View view) {
            super(view);
            previmageView = (ImageView) view.findViewById(R.id.preImageView);
            deleteImgBtn = (ImageButton) view.findViewById(R.id.preImageDeleteImgBtn);
        }
    }
}
