package com.tispoon.henen.HenenBoard;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;


import net.marusoft.search_me.R;

import java.util.List;

/**
 * Created by Yoon on 2015-12-10.
 */
public class BoardItemHorizontalImageLayoutAdapter extends RecyclerView.Adapter<BoardItemHorizontalImageLayoutAdapter.CustomViewHolder>{



    private final Context mContext;
    private final  List<String> mItems;


    public BoardItemHorizontalImageLayoutAdapter(Context context,List<String> items) {
        mContext = context;
        mItems = items;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.horizontalimageitem, parent, false);
        return new CustomViewHolder(view);
    }
    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position) {
       // Dlog.v(mItems.get(position));
        Glide.with(mContext).load(mItems.get(position)).into(holder.previmageView);
      //  holder.previmageView.setImageURI(mItems[position]);
        holder.deleteImgBtn.setVisibility(View.GONE);
    }
    @Override
    public int getItemCount() {
        return mItems.size();
    }


    public List<String> getmItems() {
        return mItems;
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        public  ImageView previmageView;
        public  ImageButton deleteImgBtn;
        public CustomViewHolder(View view) {
            super(view);
            previmageView = (ImageView) view.findViewById(R.id.preImageView);
            deleteImgBtn = (ImageButton) view.findViewById(R.id.preImageDeleteImgBtn);
        }
    }
}
